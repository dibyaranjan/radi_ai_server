# radi_ai_server

## Installing django ai-server app

### Setup
- Clone the repo
```shell
git clone https://gitlab.com/dibyaranjan/radi_ai_server.git
```

- cd to project root directory
- Create python3 virtual environment (If this command gives error, you have install python3.7-venv library)
```shell
python3 -m venv venv
```

- Activate virtual environment
```shell
source venv/bin/activate
```
- Install python dependencies
```shell
cd web/ai_server
pip3 install -r requirements.txt
```

### App deployment locally
- Set `DJANGO_ALLOWED_HOSTS` to your local IP (Replace the below IP with your local IP)
```shell script
export DJANGO_ALLOWED_HOSTS=192.168.1.6
```

- Run web app (Run this inside web/ai_server directory. This directory will have a file named `manage.py`).
You can replace the IP. If any other application is running on 8000 port, please fell free to change it.
```shell script
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py runserver 192.168.1.6:8000
```

## Endpoints
- GET: /api/test_connection/
    
    No payload needed. Will send you a 200 status if able to connect to local ai-server
    
- POST: /api/wifi/
    
    Payload: {'network_name': 'Airtel5Hz', 'password': 'abcd1234'}
    
    Send the wifi credentials to local server.
    
- GET: /api/command/
    
    Payload: {'command': 'GET STATUS'}
    
    Return the command response.
    You can test the following commands.
    
    - GET STATUS
    - RESTART AI SERVER
    - SERVICE RUNNING
    
    While displaying the response, display both key and value of JSON response.
    

## Other Information
You don't need any database setup for this. When you run this script, a sqlite db will automatically 
get created inside `web/ai_server/` directory.