"""
File:           urls.py
Author:         Dibyaranjan Sathua
Created on:     04/10/20, 3:29 AM
"""
from django.urls import re_path

from .views import TestConnectionView, WifiView, CommandView


urlpatterns = [
    re_path(r'^test_connection/$', TestConnectionView.as_view(), name='test_connection'),
    re_path(r'^wifi/$', WifiView.as_view(), name='wifi'),
    re_path(r'^command/$', CommandView.as_view(), name='command'),
]
