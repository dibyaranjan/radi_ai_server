from django.db import models


class Wifi(models.Model):
    """ Store Wifi Credential """
    network_name = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    last_updated = models.DateTimeField(auto_now=True)
