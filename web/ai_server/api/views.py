from rest_framework import status
from rest_framework import generics
from rest_framework.response import Response

from .models import Wifi


class TestConnectionView(generics.GenericAPIView):
    """ Get connection to test if phone and ai-server is on same network """

    def get(self, request, *args, **kwargs):
        """ Get request """
        data = {'message': 'Connected to ai-server successfully'}
        return Response(data=data, status=status.HTTP_200_OK)


class WifiView(generics.GenericAPIView):
    """ Store Wifi credentials """

    def post(self, request, *args, **kwargs):
        """ Post request """
        network_name = self.request.data.get('network_name')
        password = self.request.data.get('password')
        if network_name is None or password is None:
            return Response(data={'error': 'Missing post data'}, status=status.HTTP_400_BAD_REQUEST)

        credentials, created = Wifi.objects.get_or_create(network_name=network_name)
        credentials.password = password
        credentials.save()
        if created:
            data = {'message': 'Saved Wifi credentials successfully'}
        else:
            data = {'message': 'Updated Wifi credentials successfully'}

        return Response(data=data, status=status.HTTP_201_CREATED)


class CommandView(generics.GenericAPIView):
    """ Return command output """

    def get(self, request, *args, **kwargs):
        """ Get request """
        command = self.request.query_params.get('command')
        if command is None:
            return Response(data={'error': 'Missing get data'}, status=status.HTTP_400_BAD_REQUEST)

        data = {}
        if command == 'GET STATUS':
            data = {
                'server': 'Running',
                'ip-address': '192.168.1.4',
                'ai-code': True
            }

        elif command == 'RESTART AI SERVER':
            data = {
                'server': 'Restarting',
                'ram': 10240
            }

        elif command == 'SERVICE RUNNING':
            data = {
                'service': {
                    'Face Recognition': True,
                    'Object Detection': False
                },
                'detections': [500, 123]
            }

        return Response(data=data, status=status.HTTP_200_OK)
